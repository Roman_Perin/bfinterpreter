package com.teamDev.perin.translators.lexicalAnalyzer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BrainFuckScannerTest {

    Scanner scanner;

    @Before
    public void setUp() {
        scanner = new BrainFuckScanner();
    }

    @Test(expected = NullPointerException.class)
    public void testScanByNull() {
        scanner.scan(null);
    }

    @Test
    public void testScanByEmptyString() {
        char[] result = scanner.scan("");
        Assert.assertArrayEquals(new char[]{}, result);
    }

    @Test
    public void testScanByNonemptyString() {
        char[] result = scanner.scan("><+- [ ] .");
        Assert.assertArrayEquals(new char[]{'>', '<', '+', '-', '[', ']', '.'}, result);
    }



    @Test(expected = NullPointerException.class)
    public void testSetTokensByNull() {
        scanner.setTokens(null);
        scanner.scan("");
    }

    @Test
    public void testSetTokensScanByEmptyArrayOnEmptySrting() {
        scanner.setTokens(new char[0]);
        char[] result = scanner.scan("");
        Assert.assertArrayEquals(new char[0], result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetTokensScanByEmptyArray() {
        scanner.setTokens(new char[0]);
        scanner.scan("+->[]");

    }

    @Test
    public void testSetTokensByNondefaultTokens() {
        scanner.setTokens(new char[]{'>', '<', '!'});
        char[] result = scanner.scan(" >!< ");
        Assert.assertArrayEquals(new char[]{'>', '!', '<'}, result);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testSetTokensByNondefaultTokensOnCommonBFText() {
        scanner.setTokens(new char[]{'>', '<', '!'});
        char[] result = scanner.scan(" >!< .[] +-");
    }
}
