package com.teamDev.perin.translators;

import com.teamDev.perin.translators.lexicalAnalyzer.BrainFuckParserForInterpreter;
import com.teamDev.perin.translators.tape.Tape;
import com.teamDev.perin.translators.commands.CommandComponent;
import com.teamDev.perin.translators.lexicalAnalyzer.BrainFuckParser;
import com.teamDev.perin.translators.lexicalAnalyzer.BrainFuckScanner;
import com.teamDev.perin.translators.lexicalAnalyzer.Scanner;

import java.io.*;

public class BrainFuckInterpreter {

    static char[] tokenSequence;


    public static void main(String[] args) {

        String fileName = "hello.bf";
        String directory = ".\\resource\\";
        String file = directory + fileName;

        String BrainFuckProgramText = readFromFile(file);

        Scanner bfScanner = new BrainFuckScanner();
        tokenSequence = bfScanner.scan(BrainFuckProgramText);

        BrainFuckParser parser = new BrainFuckParserForInterpreter();
        CommandComponent root = parser.parse(tokenSequence);

        root.interpret(new Tape());
    }

    private static String readFromFile(String file) {
        String bfProgramText = "";

        try {

            DataInput bfReader = new DataInputStream(new FileInputStream(file));
            String line;

            while ((line = bfReader.readLine()) != null){
                bfProgramText += line;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Sorry, but file " + file + " haven't been found.");
        } catch (IOException e) {
            System.err.println("Sorry, it was a mistake during reading from the " + file + " file");
        }

        return bfProgramText;
    }

}
