package com.teamDev.perin.translators.lexicalAnalyzer;

public interface Scanner {

    public char[] scan(String programText);
    public void setTokens(char[] tokens);

}
