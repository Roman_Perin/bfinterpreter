package com.teamDev.perin.translators.lexicalAnalyzer;

import java.util.Arrays;

public class BrainFuckScanner implements Scanner {

    private char[] tokens;

    public BrainFuckScanner(){
        tokens = new char[]{'-', '+', '<', '>', '[', ']', '.'};
        Arrays.sort(tokens);
    }

    @Override
    public char[] scan(String programText) {

        String withoutSpaces = programText.replace(" ", "");
        char[] tokenSequence = withoutSpaces.toCharArray();

        scan(tokenSequence);

        return tokenSequence;
    }


    private void scan(char[] tokenSequence){

        for (int index = 0; index < tokenSequence.length; index++) {

            if (Arrays.binarySearch(tokens, tokenSequence[index]) < 0){
                throw new IllegalArgumentException((index + 1) + "'th token \""
                                                   + tokenSequence[index] + "\" is unknown.");
            }
        }
    }


    @Override
    public void setTokens(char[] tokens) {
        this.tokens = tokens;
        Arrays.sort(this.tokens);
    }


}
