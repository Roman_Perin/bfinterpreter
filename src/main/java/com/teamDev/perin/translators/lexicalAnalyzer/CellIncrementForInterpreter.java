package com.teamDev.perin.translators.lexicalAnalyzer;

import com.teamDev.perin.translators.commands.CellIncrementLeaf;
import com.teamDev.perin.translators.commands.CommandComponent;
import com.teamDev.perin.translators.tape.Tape;

public class CellIncrementForInterpreter extends CellIncrementLeaf {

    public CellIncrementForInterpreter(CommandComponent parent) {
        super(parent);
    }


    @Override
    public void interpret(Tape tape) {
        int delta = +1;
        tape.addToCurrentCell(delta);
    }

    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new CellIncrementForInterpreter(root);
    }

}
