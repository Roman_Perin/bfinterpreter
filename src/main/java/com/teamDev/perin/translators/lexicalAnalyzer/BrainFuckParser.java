package com.teamDev.perin.translators.lexicalAnalyzer;

import com.teamDev.perin.translators.commands.*;

import java.util.ArrayDeque;
import java.util.Deque;

public abstract class BrainFuckParser {

    private CommandComponent        root  = new RootComposite(null);
    private Deque<CommandComponent> loops = new ArrayDeque<>();
    private int tokenPosition = 1;

    private PointerIncrementLeaf pointerIncrement;
    private PointerDecrementLeaf pointerDecrement;
    private CellIncrementLeaf    cellIncrement;
    private CellDecrementLeaf    cellDecrement;
    private PrintCellLeaf    printCell;
    private RootComposite    loopBegin;
    private LoopEndLeaf      loopEnd;


    public BrainFuckParser() {
        initialize();
    }

    public CommandComponent parse(char[] tokenSequence){

        for (char token : tokenSequence) {

            if        (token == '>'){
                root.add(pointerIncrement.newInstance(root));
            } else if (token == '<'){
                root.add(pointerDecrement.newInstance(root));
            } else if (token == '+'){
                root.add(cellIncrement.newInstance(root));
            } else if (token == '-'){
                root.add(cellDecrement.newInstance(root));
            } else if (token == '.'){
                root.add(printCell.newInstance(root));
            } else if (token == '['){
                setLocalRoot(loopBegin.newInstance(root));
            } else if (token == ']'){
                if (!loops.isEmpty()){
                    root.add(loopEnd.newInstance(root));
                    root = loops.pop();
                } else {
                    throw new IllegalArgumentException("\']\' at position " + tokenPosition +
                                                       "doesn't have corresponding left bracket");
                }
            } else {
                throw new IllegalArgumentException("There is unknown token " + token + " at position " + tokenPosition);
            }
            tokenPosition++;
        }
        checkOnClosedBrackets();

        return root;
    }


    public void setPointerIncrement(PointerIncrementLeaf pointerIncrement) {
        this.pointerIncrement = pointerIncrement;
    }
    public void setPointerDecrement(PointerDecrementLeaf pointerDecrement) {
        this.pointerDecrement = pointerDecrement;
    }
    public void setCellIncrement(CellIncrementLeaf cellIncrement) {
        this.cellIncrement = cellIncrement;
    }
    public void setCellDecrement(CellDecrementLeaf cellDecrement) {
        this.cellDecrement = cellDecrement;
    }
    public void setPrintCell(PrintCellLeaf printCell) {
        this.printCell = printCell;
    }
    public void setLoopBegin(RootComposite loopBegin) {
        this.loopBegin = loopBegin;
    }
    public void setLoopEnd(LoopEndLeaf loopEnd) {
        this.loopEnd = loopEnd;
    }

    private void setLocalRoot(CommandComponent localRoot) {
        root.add(localRoot);
        loops.push(root);
        root = localRoot;
    }


    private void checkOnClosedBrackets() {
        if (!loops.isEmpty()){
            int unclosedBrackets = loops.size();
            throw new IllegalArgumentException(unclosedBrackets + " brackets aren't closed.");
        }
    }


    /*This is factory method. Use all set-methods for initialization*/
    protected abstract void initialize();

}
