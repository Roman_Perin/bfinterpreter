package com.teamDev.perin.translators.lexicalAnalyzer;

import com.teamDev.perin.translators.commands.*;

public class BrainFuckParserForInterpreter extends BrainFuckParser {

    public BrainFuckParserForInterpreter() {
        super();
    }

    @Override
    protected void initialize() {
        setCellDecrement(new CellDecrementForInterpreter(null));
        setCellIncrement(new CellIncrementForInterpreter(null));
        setLoopBegin(new LoopStartForInterpreter(null));
        setPointerDecrement(new PointerDecrementForInterpreter(null));
        setPointerIncrement(new PointerIncrementForInterpreter(null));
        setPrintCell(new PrintCellAsACharForInterpreter(null));
        setLoopEnd(new LoopEndForInterpreter(null));
    }

}
