package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

public class PointerDecrementForInterpreter extends PointerDecrementLeaf {

    public PointerDecrementForInterpreter(CommandComponent parent) {
        super(parent);
    }


    @Override
    public void interpret(Tape tape) {
        int delta = -1;
        tape.movePointer(delta);
    }


    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new PointerDecrementForInterpreter(root);
    }
}
