package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

public class PrintCellAsACharForInterpreter extends PrintCellLeaf {

    public PrintCellAsACharForInterpreter(CommandComponent parent) {
        super(parent);
    }

    @Override
    public void interpret(Tape tape) {
        char currentCell = (char)tape.getCurrentCellValue();
        System.out.print(currentCell);
    }

    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new PrintCellAsACharForInterpreter(root);
    }
}
