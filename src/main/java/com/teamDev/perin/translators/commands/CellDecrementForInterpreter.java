package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

public class CellDecrementForInterpreter extends CellDecrementLeaf {

    public CellDecrementForInterpreter(CommandComponent parent) {
        super(parent);
    }


    @Override
    public void interpret(Tape tape) {
        int delta = -1;
        tape.addToCurrentCell(delta);
    }


    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new CellDecrementForInterpreter(root);
    }
}
