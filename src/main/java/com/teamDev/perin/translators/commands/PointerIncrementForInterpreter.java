package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

public class PointerIncrementForInterpreter extends PointerIncrementLeaf {

    public PointerIncrementForInterpreter(CommandComponent parent) {
        super(parent);
    }

    @Override
    public void interpret(Tape tape) {
        int delta = +1;
        tape.movePointer(delta);
    }

    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new PointerIncrementForInterpreter(root);
    }
}
