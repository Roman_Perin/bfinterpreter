package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

import java.util.ListIterator;

public class RootComposite extends CommandComponent{

    private ListIterator<CommandComponent> commandsIterator = iterator();


    public RootComposite(CommandComponent parent) {
        super(parent);
    }


    @Override
    public void interpret(Tape tape) {

        for (CommandComponent commands : this) {
            commands.interpret(tape);
        }
    }


    @Override
    public void add(CommandComponent component) {
        commandsIterator.add(component);
    }


    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new RootComposite(root);
    }


}


