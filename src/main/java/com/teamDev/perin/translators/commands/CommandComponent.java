package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public abstract class CommandComponent implements Iterable<CommandComponent>{

    private CommandComponent parent;
    private List<CommandComponent> components = new LinkedList<>();


    public CommandComponent(CommandComponent parent){
        this.parent = parent;
    }


    @Override
    public ListIterator<CommandComponent> iterator(){
        return components.listIterator();
    }


    public void add(CommandComponent component){
        throw new UnsupportedOperationException();
    }


    public CommandComponent getParent(){
        return parent;
    }

    public abstract CommandComponent newInstance(CommandComponent root);

    public abstract void interpret(Tape tape);

}
