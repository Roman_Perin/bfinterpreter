package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

public class LoopEndForInterpreter extends LoopEndLeaf {

    public LoopEndForInterpreter(CommandComponent parent) {
        super(parent);
    }


    @Override
    public void interpret(Tape tape) {
        //NOP
    }


    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new LoopEndForInterpreter(root);
    }
}
