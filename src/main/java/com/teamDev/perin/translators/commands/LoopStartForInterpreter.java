package com.teamDev.perin.translators.commands;

import com.teamDev.perin.translators.tape.Tape;

import java.util.ListIterator;



public class LoopStartForInterpreter extends RootComposite{

    private ListIterator<CommandComponent> commandsIterator = iterator();


    public LoopStartForInterpreter(CommandComponent parent) {
        super(parent);
    }


    @Override
    public void add(CommandComponent component) {
        commandsIterator.add(component);
    }


    @Override
    public void interpret(Tape tape) {

        while (tape.getCurrentCellValue() != 0){
            super.interpret(tape);
        }
    }


    @Override
    public CommandComponent newInstance(CommandComponent root) {
        return new LoopStartForInterpreter(root);
    }


}
