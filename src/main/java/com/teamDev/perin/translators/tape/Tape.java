package com.teamDev.perin.translators.tape;

public class Tape {

    private final int MAX_CELL_VALUE = 256;
    private final int MIN_CELL_VALUE = 0;
    private int[] tape = new int[32];
    private int pointer = 0;

    public void movePointer(int delta) {
        pointer += delta;
        moveChecking();
    }

    public void addToCurrentCell(int delta) {
        tape[pointer] += delta;
        cellValueChecking();
    }

    public int getCurrentCellValue(){
        return tape[pointer];
    }

    private void moveChecking() {
        if (pointer < 0) {
            throw new IndexOutOfBoundsException("pointer become less than zero. " + "pointer = " + pointer);
        } else if (pointer >= tape.length) {
            scaleUpTapeLength();
        }
    }

    private void scaleUpTapeLength() {
        int[] temp = new int[tape.length * 2];
        System.arraycopy(tape, 0, temp, 0, tape.length);
        tape = temp;
    }

    private void cellValueChecking() {
        int cellValue = tape[pointer];

        if (cellValue < MIN_CELL_VALUE || cellValue > MAX_CELL_VALUE) {
            throw new IndexOutOfBoundsException("cell value felt outside the limits. Limits are: (" + MIN_CELL_VALUE +
                                                " <= cell_value <=" + MAX_CELL_VALUE + ") but cell_value = " + cellValue);

        }
    }


}
